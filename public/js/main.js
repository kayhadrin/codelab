navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;

var constraints = {
	video: true
	//, audio: true
};
var stream;

function successCallback (localMediaStream) {
	stream = localMediaStream; // stream available to console
	var video = document.querySelector("video");
	video.src = window.URL.createObjectURL(localMediaStream);
	video.height = window.innerHeight;
	video.play();
}

function errorCallback (error) {
	console.log("navigator.getUserMedia error: ", error);
}

navigator.getUserMedia(constraints, successCallback, errorCallback);